# -*- coding: utf-8 -*-
# @Author : Romain FONCIER

# The total is right - The goal is to arrive at a chosen number (from 1 to 999) using the four basic arithmetic operations (+, −, × and ÷) applied to six numbers chosen by the user. 

# The purpose of this class is to illustrate the concept of Backtracking via the "The total is right" problem.

import sys, copy

class TotalIsRight:
    def __init__(self, targetNumber, listOperand):
        self.target = targetNumber
        self.operand = listOperand
        self.operator = ['+','-','*','/']
        self.currentSize = len(self.operand)
        self.combi = []
        self.bestCombi = []
        self.bestRes = 0
        self.bestDiff = sys.maxsize
        self.searchSolution(self.currentSize)

    def getMax(self):
        return self.bestRes

    def printRes(self):
        print('--- Target : '+str(self.target)+' ---\n')
        for i in range(len(self.bestCombi)):
            print(self.bestCombi[i]+'\n')
        print('--- Best result : '+str(self.bestRes)+' --- Ecart : '+str(self.bestDiff)+' ---\n')

    def ecart(self, res):
        if res > self.target:
            return res - self.target
        else:
            return self.target - res

    def save(self):
        self.bestRes = self.operand[0]
        self.bestCombi = copy.copy(self.combi)

    def operation(self, op1, op2, operator):
        if operator == '+':
            return op1 + op2
        elif operator == '-':
            return op1 - op2
        elif operator == '*':
            return op1 * op2
        else:
            if op2 != 0:
                return op1 / op2
            else:
                return 0

    def searchSolution(self, size):
        if size == 1:
            if self.operand[0] == self.target:
                self.save()
                return True
            elif (self.ecart(self.operand[0]) < self.bestDiff):
                self.save()
                self.bestDiff = self.ecart(self.bestRes)
                return False
            else:
                return False
        else:
            for i in range (size):
                for j in range (i+1,size):
                    op1 = self.operand[i]
                    op2 = self.operand[j]
                
                    for o in range (len(self.operator)):
                        currentRes = self.operation(op1, op2, self.operator[o])
                        self.operand[i] = currentRes
                        self.operand[j] = self.operand[size-1]
                        if (currentRes > 0) and (currentRes < (self.target + self.bestDiff)):
                            self.combi.append(str(op1)+' '+str(self.operator[o])+' '+str(op2)+' = '+str(currentRes))
                            if (self.searchSolution(size-1)):
                                return True
                            else:
                                self.combi.pop()

                    self.operand[i] = op1
                    self.operand[j] = op2
            return False

