# -*- coding: utf-8 -*-

# @author : Romain FONCIER

""" A polygon can be defined (as illustrated above) as a geometric object "consisting of a number of points (called vertices)
    and an equal number of line segments (called sides), namely a cyclically ordered set of points in a plane, with no three 
    successive points collinear, together with the line segments joining consecutive pairs of the points. In other words, 
    a polygon is closed broken line lying in a plane" (Coxeter and Greitzer 1967, p. 51). """

from node import Node
#from point import Point
from pointr0 import Point

class Polygon:
        
    def __init__(self):
        """ In this case, we implement a circularly doubly linked list.
            Thus, the constructor create a head's list which references nothing (None) and
            of length == 0 """
        self.head = None
        self.length = 0                 # Note : It's easier to add the length directly to the head's list

    def length(self):
        """ Return length with length() method (for user -> ADT) """
        return self.length
    
    def constructorFirst(self,newPoint):
        """ Create the first point """
        self.head = Node(newPoint)
        self.head.setNext(self.head)
        self.head.setPrevious(self.head)
        self.length += 1
    
    def isEmpty(self):
        """ Return True if self.head worth None """
        return self.head == None                # or return self.length == 0
        
    def firstPoint(self):
        """ Return the first point in the list """
        return self.head.getData()

    def lastPoint(self):
        """ Return the last point in the list """
        return self.head.getPrevious().getData()
        
    def add(self,newPoint):
        """ Add the new item at the head of list """
        if self.isEmpty():
            self.constructorFirst(newPoint)
        else:
            temp = Node(newPoint,self.head.getPrevious(),self.head)
            self.head.setPrevious(temp)
            temp.getPrevious().setNext(temp)
            self.head = temp
            self.length += 1

    def addAfter(self,target,newPoint):
        """ Add the new item after the node given in parameter """
        temp = Node(newPoint,target,target.getNext())
        temp.getNext().setPrevious(temp)
        target.setNext(temp)
        self.length += 1

    def addEnd(self,newPoint):
        """ Add the new item at the end of list or add a new node if is empty """
        if self.isEmpty():              # Note : the user may want to add a point at the end of list although is empty
            self.constructorFirst(newPoint)
        else:
            self.addAfter(self.head.getPrevious(),newPoint)

    def search(self,searchPoint):
        """ Return True if the node containing the searchPoint is found and False otherwise """
        return self.searchNodeOf(searchPoint) != None
        
    def searchNodeOf(self,searchPoint):
        """ Return the node containing the searchPoint or None if not found """
        if not self.isEmpty():
            current = self.head
            while current.getNext() != self.head and current.getData() != searchPoint:
                current = current.getNext()
        if current.getData() != searchPoint or self.isEmpty():
             current = None
        return current

    def remove(self,delPoint):
        """ Delete the node containing delPoint if found """
        node = self.searchNodeOf(delPoint)
        if node != None:
            if self.length > 1:
                self.removeNode(node)
            elif self.length == 1:
                self.head = None
            self.length -= 1

    def removeNode(self,delNode):
        """ Delete the node enter in parameter """
        delNode.getNext().setPrevious(delNode.getPrevious())
        delNode.getPrevious().setNext(delNode.getNext())
        if delNode == self.head:
            self.head = delNode.getNext()

    def removeFirst(self):
        """ Delete the first node in the list """
        point = self.firstPoint()
        self.remove(point)

    def removeLast(self):
        """ Delete the last node in the list """
        point = self.lastPoint()
        self.remove(point)

    def collinear(self,head_node):
        """ Return 0 if collinear, 1 for left bend and -1 for right bend """
        n1 = head_node             
        n2 = n1.getNext()
        n3 = n2.getNext()
        # Most explicit!
        p1 = n1.getData()
        p2 = n2.getData()
        p3 = n3.getData()
        
        # print(p1,p2,p3)           only for test
        s = ((p2.getX()-p1.getX())*(p3.getY()-p1.getY()))-((p2.getY()-p1.getY())*(p3.getX()-p1.getX()))
        
        if s > 0:
            return 1
        elif s < 0:
            return -1
        else:
            return 0
   
    def isConvex(self):
        """ Return True if the polygon is convex otherwise False """
        if self.isPolygon():            # By precautions but obviously I consider the user tests the convexity of an polygon!
            start = self.head
            axis = self.collinear(start)
            status = True
            while start.getNext() != self.head and status == True:
                status = axis == self.collinear(start.getNext())
                start = start.getNext()
            return status
        else:
            return False
    
    def isPolygon(self):
        """ Return True if the polygon doesn't have three successive points collinear otherwise False """
        if self.length > 2:
            start = self.head
            status = self.collinear(start) != 0
            while start.getNext() != self.head and status == True:
                status = self.collinear(start.getNext()) != 0
                start = start.getNext()
            return status
        else:
            return False
    
    def isConcave(self):
        """ A concave polygon is a polygon that is not convex """
        return self.isPolygon() and self.isConvex() != True

    def __repr__(self):
        """ Displaying the polygon points as a point list """
        res = "["
        current = self.head
        for i in range(self.length):
            res += current.printData()
            if i != self.length-1:
                res +=", "
            current = current.getNext()
        if self.length > 2:
            res += "] - Polygon with "+str(self.length)+" side(s)"
        else:
            res += "] - Polygon with "+str(self.length)+" point(s)"
        return res

if __name__ == "__main__":
    print("--------------------------------\n\n")
    p = Polygon()
    print("p is empty\n")
    print(p)
    p1 = Point(1,1)
    p2 = Point(2,0)
    p3 = Point(4,2)
    p4 = Point(3,4)
    p5 = Point(0,3)
    p.addEnd(p1)
    p.addEnd(p2)
    p.addEnd(p3)
    p.addEnd(p4)
    p.addEnd(p5)
    print("\n")
    print("p contains 5 points\n")
    print(p)
    if p.isConvex():
        print("P is convex\n")
    else:
        print("P isn't convex\n")
    if p.isPolygon():
        print("P is a polygon!\n")
    else:
        print("P isn't a polygon!\n")


    p6 = Point(2,1)
    p.removeLast()
    p.addEnd(p6)
    print("\n")
    print(p)
    if p.isConvex():
        print("P is convex\n")
    elif p.isConcave():
        print("P is concave\n")
    if p.isPolygon():
        print("P is polygon!\n")
    else:
        print("P isn't a polygon!\n")

    p7 = Point(0,2)
    p.removeLast()
    p.addEnd(p7)
    print("\n")
    print(p)
    if p.isConvex():
        print("P is convex\n")
    elif p.isConcave():
        print("P is concave\n")
    else:
        print("P isn't a polygon")
    if p.isPolygon():
        print("P is polygon!\n")
    else:
        print("P isn't a polygon!\n")

    """
    p1 = Point(1,1)
    p2 = Point(1.5,3)
    p3 = Point(4,4)
    p4 = Point(5,2)
    p5 = Point(4,0.5)
    p.add(p1)
    p.add(p2)
    p.add(p3)
    p.add(p4)
    p.add(p5)
    print("\n")
    print("p contains 5 points\n")
    print(p)
    p6 = Point(7,2)
    p.addEnd(p6)
    print("\n")
    print("p contains 6 points\n")
    print(p)
    p.removeFirst()
    print("\n")
    print("p contains 5 points\n")
    print(p)
    p.removeLast()
    print("\n")
    print("p contains 4 points\n")
    print(p)
    p.remove(p3)
    print("\n")
    print("p contains 3 points\n")
    print(p)
    i = p.searchNodeOf(p2)
    p.addAfter(i,Point(0,0))
    print("\n")
    print("p contains 4 points\n")
    print(p)
    p.removeLast()
    p.removeLast()
    p.removeFirst()
    print("\n")
    print("p contains 1 point\n")
    print(p)
    p.removeFirst()
    print("\n")
    print("p contains 0 point\n")
    print(p)
    p.addEnd(p1)
    p.addEnd(p2)
    p.addEnd(p3)
    p.addEnd(p4)
    p.addEnd(p5)
    print("\n")
    print("p contains 5 points\n")
    print(p)
    """

