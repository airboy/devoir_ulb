# -*- coding: utf-! -*-

# @author : Romain FONCIER

""" A point is representing a location in (x,y) coordinate space """

class Point:
    
    def __init__(self,x=0,y=0):
    # The X value (by default = 0)
        self.__x = x

    # The Y value (default = 0)
        self.__y = y

    def getX(self):
        return self.__x

    def getY(self):
        return self.__y

    def getCoordinate(self):
        """ Return a tuple of coordinate for this point """
        return (self.__x,self.__y)

    def __repr__(self):
        """ Return a string representation of this point and its location in (x,y) coordinate space """
        return "(%s;%s)" % (self.__x, self.__y)

    def changeCoord(self,newx,newy):
        """ Move this point to the specified location in (newx,newy) coordinate plane """
        self.__x = newx
        self.__y = newy

