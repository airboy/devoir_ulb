# -*- coding: utf-! -*-

# @author : Romain FONCIER

""" The polar coordinates r (the radial coordinate) and θ (the angular coordinate, often called the polar angle) are defined in terms of Cartesian coordinates by x=rcosθ, y=rsinθ, where r is the radial distance from the origin, and θ is the counterclockwise angle from the x-axis. Weisstein, Eric W. "Polar Coordinates." From MathWorld--A Wolfram Web Resource. """

import math

class Point:
    
    def __init__(self,x=0,y=0):
        # r, the radial coordinate
        self.__r = math.hypot(x,y)
        # θ, the angular coordinate
        if (x > 0 and y >= 0):
            self.__t = math.atan2(y,x)
        elif (x > 0 and y < 0):
            self.__t = math.atan2(y,x)
        elif (x < 0):
            self.__t = math.atan2(y,x)
        elif (x == 0 and y > 0):
            self.__t = math.pi/2
        elif (x == 0 and y <0):
            self.__t = (math.pi*3)/2
        else:
        # Default constructor
            self.__t = 0.0

    def getX(self):
        #return round(self.__r*math.cos(self.__t),2)    In practice, I rounded the values to be sure of getting a best result
        return self.__r*math.cos(self.__t)

    def getY(self):
        #return round(self.__r*math.sin(self.__t),2)    Idem above
        return self.__r*math.sin(self.__t)

    def getCoordinate(self):
        """ Return a tuple of coordinates for this point """
        return (self.getX(),self.getY())


    def getPolarCoordinate(self):
        """ Return a tuple of polar coordinates for this point """
        return (self.__r,self.__t)

    def setX(self, newx):
        y = self.__r*math.sin(self.__t)
        self.__init__(newx,y)

    def setY(self, newy):
        x = self.__r*math.cos(self.__t)
        self.__init__(x,newy)

    def isEqual(self,point):
        """ Return True if the current object is equal to the point enter in parameters """
        return (self.__r == point.__r and self.__t == point.__t)

    def __repr__(self):
        """ Return a string representation of this point and its location in (x,y) coordinate space """
        return "(%.1f;%.1f)" % (self.getCoordinate())

    def polarRepr(self):
        """ Return a string representation of this point and its location in the polar coordinate system (r,θ) """
        return "(%s;%s)" % (self.__r, self.__t)


"""
if __name__ == "__main__":
    print("--------------------------------------------------------------\n")
    p1 = Point(1,1)
    p2 = Point(2,6)
    p3 = Point(-2,4)
    p4 = Point(-1,-1)
    p5 = Point(1,1)
    print("trois points : ")
    print(p1,p3,p5)
    print("\n Coordonnées de p4 avec getX et getY\n")
    print(p4.getX(), p4.getY())
    print("\n coordonnées de p2 avec getCoordinate et getPolarCoordinate\n")
    print(p2.getCoordinate())
    print(p2.getPolarCoordinate())
    print("\n changement de coordonnées pour p4 (x et y)\n")
    p4.setX(-2)
    p4.setY(2)
    print(p4)
    print("\n test égalité entre p1 et p5\n")
    print(p1.isEqual(p5))
    print("\n test égalité entre p2 et p4\n")
    print(p2.isEqual(p4))
    print("\n p1 repr et polarRepr\n")
    print(p1)
    print(p1.polarRepr())
"""

