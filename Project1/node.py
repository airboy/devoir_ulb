# -*- coding: utf-8 -*-

# @author : Romain FONCIER

""" A node is a record consisting of one field of data and 
    two links : one to next node and one to previous node 
    (In this case : we implement a circularly doubly linked list) """

#from point import Point
from pointr0 import Point

class Node:

    def __init__(self,data=Point(),previous=None,next=None):
        """ Node Constructor """
        self.data = data
        self.next = next
        self.previous = previous

    def getData(self):
        return self.data

    def getNext(self):
        return self.next

    def getPrevious(self):
        return self.previous

    def setData(self,newData):
        self.data = newData

    def setNext(self,newNext):
        self.next = newNext

    def setPrevious(self,newPrevious):
        self.previous = newPrevious

    def printData(self):
        return str(self.data)


