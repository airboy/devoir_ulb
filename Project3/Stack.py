class Stack:
    def __init__(self):
        self.items = []
	
    def isEmpty(self):
        return self.items == []
	
    def push(self, item):
        self.items.append(item)
	
    def pop(self):
        if (not self.isEmpty()):
            return self.items.pop()
	
    def top(self):
        if (not self.isEmpty()):
            return self.items[len(self.items)-1]
        else:
            return 'Stack is empty'

    def size(self):
        return len(self.items)
	
    def __str__(self):
        res = ""
        for i in self.items:
            res += str(i) + "  | "
        return res

