# -*- coding: utf-8 -*-
#
# @Author: R. FONCIER
# @Date: 23/04/2012

from BinaryTree import BinaryTree
from Stack import Stack
import math

class AlgebraicTree:
    def __init__(self, expression):
        """ Algebraic Tree constructor """

        self.e = expression
        self.opPrior = {'*': 3, '/': 3, '+': 2, '-': 2, '(': 1}
        self.postFix = Stack() 
        self.infixToPostfix(self.e)
        self.Tree = None
        self.expTree()
       
        
    def infixToPostfix(self, exp):
        """ Convert the algebraic expression from infix to postfix notation """

        s = Stack()
        num = []
        for i in range(len(exp)):
            if (exp[i].isdigit()):
                num.append(exp[i])
            elif (exp[i] == '('):
                s.push(exp[i])
            elif (exp[i] == ')'):
                topExp = s.pop()
                if (num != []):
                    self.postFix.push(''.join(num))
                    num = []
                while (topExp != '('):
                    self.postFix.push(topExp)
                    topExp = s.pop()
            else:
                if (num != []):
                    self.postFix.push(''.join(num))
                    num = []
                while (not s.isEmpty()) and (self.opPrior[s.top()] >= self.opPrior[exp[i]]):
                    self.postFix.push(s.pop())
                s.push(exp[i])
        if (num != []):
            self.postFix.push(''.join(num))
            num = []
        while not s.isEmpty():
            self.postFix.push(s.pop())


    def expTree(self):
        """ Create a BinaryTree from expression in postfix notation """
        
        if (not self.postFix.isEmpty()):
            self.Tree = BinaryTree(self.postFix.pop())
        if (not self.Tree.getRootVal().isdigit()):
            self.constructorTree(self.Tree)


    def constructorTree(self, tree):
        """ Construct the tree """

        if (not self.postFix.isEmpty()):
            # Right sub tree
            tree.modifyRight(self.postFix.pop())
            if (not tree.getRightChild().getRootVal().isdigit()):
                self.constructorTree(tree.getRightChild())
            
            # Left sub tree
            tree.modifyLeft(self.postFix.pop())
            if (not tree.getLeftChild().getRootVal().isdigit()):
                self.constructorTree(tree.getLeftChild())
 
    
    def cal(self, op1, op2, operator):
        """ Return the result of op1.operator(op2) """
        
        if (operator == '+'):
            return (op1+op2)
        elif (operator == '-'):
            return (op1-op2)
        elif (operator == '*'):
            return (op1*op2)
        else:
            return (op1/op2)

    
    def evaluate(self):
        """ Return the result of expressionTree """

        return self.evaluateTree(self.Tree)

    def evaluateTree(self, tree):
        """ Return the result of expression tree enter in parameters """

        root = tree.getRootVal()
        if (not root.isdigit()):
            a = tree.getLeftChild().getRootVal()
            b = tree.getRightChild().getRootVal()
            
            if (a.isdigit()):
                a = int(a)
            else:
                a = self.evaluateTree(tree.getLeftChild())

            if (b.isdigit()):
                b = int(b)
            else:
                b = self.evaluateTree(tree.getRightChild())
            
            return (self.cal(a,b,root))
        else:
            return (int(root))

    def __repr__(self):
        """ Return the expresiion in infix notation from binary tree """
        return self.Tree.inorderToString('(', ')')


    def __add__(self, other):
        """ Return a new expression resulting of operator + overloading """
        newExp = ''.join([self.__repr__(), '+', other.__repr__()])
        return AlgebraicTree(newExp)

    def __sub__(self, other):
        """ Return a new expression resulting of operator - overloading """
        newExp = ''.join([self.__repr__(), '-', other.__repr__()])
        return AlgebraicTree(newExp)

    def __mul__(self, other):
        """ Return a new expression resulting of operator * overloading """
        newExp = ''.join([self.__repr__(), '*', other.__repr__()])
        return AlgebraicTree(newExp)

    def __truediv__(self, other):
        """ Return a new expression resulting of operator / overloading """
        newExp = ''.join([self.__repr__(), '/', other.__repr__()])
        return AlgebraicTree(newExp)

    def __pow__(self, other):
        """ Return a new expression resulting of operator ** overloading """
        i = math.floor(other.evaluate())
        newExp = []
        if i > 0:
            for ii in range (i):
                newExp.append(self.__repr__())
                if (ii<(i-1)):
                    newExp.append('*')
        else:
            newExp.append('1')
        newExp = ''.join(newExp)
        return AlgebraicTree(newExp)

"""
if (__name__ == '__main__'):
    print('----------')
    e1 = '2'
    e2 = '1/65'
    e5 = '16/4'

    
    e3 = '4/(((2+5)*10)+4)'
    e4 = '(2+5)/(2+5)'
    e5 = '16/4'
    e6 = '16*4'
    e7 = '16-4'
    e8 = '1/2'
    e9 = '((16-4)*((((2+5)/(2+5))*(16*4))/(4/(((2+5)*10)+4))))'
    
    ea1 = AlgebraicTree(e1)
    ea2 = AlgebraicTree(e2)
    ea5 = AlgebraicTree(e5)
    print(ea1.evaluate())
    print(ea2.evaluate())
    print(math.floor(ea1.evaluate()))
    print(math.floor(ea2.evaluate()))
    res = ea1**ea2
    print(res)

    
    ea3 = AlgebraicTree(e3)
    ea4 = AlgebraicTree(e4)
    ea5 = AlgebraicTree(e5)
    ea6 = AlgebraicTree(e6)
    ea7 = AlgebraicTree(e7)
    ea8 = AlgebraicTree(e8)
    ea9 = AlgebraicTree(e9)

    res = ea1**ea2
    print(res)
    print('\n')
    res = ea2**ea1
    print(res)
    print('\n')
    res = ea3**ea2
    print(res)
    print('\n')
    res = ea4**ea3
    print(res)
    print('\n')
    res = ea5**ea5
    print(res)
    print('\n')
    res = ea6**ea6
    print(res)
    print('\n')
    res = ea7**ea8
    print(res)
    print('\n')

    print(ea1)
    print('\n')
    print(ea2)
    print('\n')
    print(ea3)
    print('\n')
    print(ea4)
    print('\n')
    print(ea5)
    print('\n')
    print(ea6)
    print('\n')
    print(ea7)
    print('\n')
    print(ea8)
    print('\n')
    print(ea9)
    print('\n')


    e = '10+(5*2)'
    #e = '(10+5)*3'
    e1 = '(18*12)+14'
    ea = AlgebraicTree(e)
    print(ea.postFix)
    ea1 = AlgebraicTree(e1)
    print('\n')
    print(ea.Tree)
    print(ea)
    print(ea.evaluate())
    
    print('\n')
    res = ea+ea1
    print(res)
    print(res.evaluate())
    
    print('\n')
    res = ea-ea1
    print(res)
    print(res.evaluate())
    
    print('\n')
    res = ea*ea1
    print(res)
    print(res.evaluate())

    print('\n')
    res = ea/ea1
    print(res)
    print(res.evaluate())

    print('\n')
    ea2 = AlgebraicTree('1+2')
    print(ea2)
    print(ea2.evaluate())
    res = ea**ea2
    print(res)
    print(res.evaluate())
"""
    
