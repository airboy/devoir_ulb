import unittest 
# How to use unittest: http://docs.python.org/py3k/library/unittest.html?highlight=unittest#unittest
# setUp, tearDown
# When a setUp() method is defined, the test runner will run that method prior to each test. Likewise, if a tearDown() method is defined, the test runner will invoke that method after each test. In the example, setUp() was used to create a fresh sequence for each test.

# assertEqual(), assertTrue(), assertRaises()
# The crux of each test is a call to assertEqual() to check for an expected result; assertTrue() to verify a condition; or assertRaises() to verify that an expected exception gets raised. These methods are used instead of the assert statement so the test runner can accumulate all test results and produce a report.

from AlgebraicTree import AlgebraicTree
import random

class TestAlgebraicTree(unittest.TestCase):
	def addToExprsAndATs(self,expression):
		self.exprs.append(expression)
		self.ats.append(AlgebraicTree(expression))

	def setUp(self):
		self.numberOfTestsPerLoop = 200000
		
		self.exprs = []
		self.ats = []
		
		self.addToExprsAndATs("2")
		self.addToExprsAndATs("(2+5)")
		self.addToExprsAndATs("(4/(((2+5)*10)+4))")
		self.addToExprsAndATs("((2+5)/(2+5))")
		self.addToExprsAndATs("(16/4)")
		self.addToExprsAndATs("(16-4)")
		self.addToExprsAndATs("(16*4)")
		self.addToExprsAndATs("(1/2)")
		self.addToExprsAndATs("((16-4)*((((2+5)/(2+5))*(16*4))/(4/(((2+5)*10)+4))))")
		# self.addToExprsAndATs("(2)")
		# self.addToExprsAndATs("((2)+(5))")
		# self.addToExprsAndATs("(((2)+(5))/((2)+(5)))")
		# self.addToExprsAndATs("((16)/(4))")
		# self.addToExprsAndATs("((16)-(4))")
		# self.addToExprsAndATs("((16)*(4))")
		# self.addToExprsAndATs("((1)/(2))")
		
		
	# def tearDown(self):
	
	def test__repr__(self):
		for i in range(len(self.ats)):
			self.assertEqual(self.ats[i].__repr__(), self.exprs[i])

	def test_evaluate(self):
		for i in range(len(self.ats)):
			self.assertEqual(self.ats[i].evaluate(), eval(self.exprs[i]))
			
	def loopGenericTestForAnOperator(self,operator,operatorAsString):
		for i in range(1,self.numberOfTestsPerLoop):
			randomIndex1 = random.randint(1, len(self.ats)-1)	
			randomIndex2 = random.randint(1, len(self.ats)-1)	
			test = operator(self.ats[randomIndex1],self.ats[randomIndex2])
			self.assertEqual(test.evaluate(),operator(self.ats[randomIndex1].evaluate(),self.ats[randomIndex2].evaluate()))
			self.assertEqual(test.__repr__(), "("+self.ats[randomIndex1].__repr__()+operatorAsString+self.ats[randomIndex2].__repr__()+")")
			
	def test__add__(self):
		for i in range(1,self.numberOfTestsPerLoop):
			randomIndex1 = random.randint(1, len(self.ats)-1)	
			randomIndex2 = random.randint(1, len(self.ats)-1)	
			test = self.ats[randomIndex1]+self.ats[randomIndex2]
			self.assertEqual(test.evaluate(),self.ats[randomIndex1].evaluate()+self.ats[randomIndex2].evaluate())
			self.assertEqual(test.__repr__(), "("+self.ats[randomIndex1].__repr__()+"+"+self.ats[randomIndex2].__repr__()+")")

		
	def test__sub__(self):
		for i in range(1,self.numberOfTestsPerLoop):
			randomIndex1 = random.randint(1, len(self.ats)-1)	
			randomIndex2 = random.randint(1, len(self.ats)-1)	
			test = self.ats[randomIndex1]-self.ats[randomIndex2]
			self.assertEqual(test.evaluate(),self.ats[randomIndex1].evaluate()-self.ats[randomIndex2].evaluate())
			self.assertEqual(test.__repr__(), "("+self.ats[randomIndex1].__repr__()+"-"+self.ats[randomIndex2].__repr__()+")")

	def test__mul__(self):
		for i in range(1,self.numberOfTestsPerLoop):
			randomIndex1 = random.randint(1, len(self.ats)-1)	
			randomIndex2 = random.randint(1, len(self.ats)-1)	
			test = self.ats[randomIndex1]*self.ats[randomIndex2]
			self.assertEqual(test.evaluate(),self.ats[randomIndex1].evaluate()*self.ats[randomIndex2].evaluate())
			self.assertEqual(test.__repr__(), "("+self.ats[randomIndex1].__repr__()+"*"+self.ats[randomIndex2].__repr__()+")")

	def test__div__(self):
		for i in range(1,self.numberOfTestsPerLoop):
			randomIndex1 = random.randint(1, len(self.ats)-1)	
			randomIndex2 = random.randint(1, len(self.ats)-1)	
			test = self.ats[randomIndex1]/self.ats[randomIndex2]
			self.assertEqual(test.evaluate(),self.ats[randomIndex1].evaluate()/self.ats[randomIndex2].evaluate())
			self.assertEqual(test.__repr__(), "("+self.ats[randomIndex1].__repr__()+"/"+self.ats[randomIndex2].__repr__()+")")

	def test__pow__(self):
		for i in range(1,self.numberOfTestsPerLoop):
			randomValue = random.randint(1, 10)
			at12 = self.ats[0]**(AlgebraicTree(str(randomValue)))			
			self.assertEqual(at12.evaluate(),self.ats[0].evaluate()**randomValue)
		
		#0 by 0 should raise exception		
		# self.assertRaises(Exception,AlgebraicTree("0")**AlgebraicTree("0"))
				
		for i in range(len(self.ats)):
			res = self.ats[i]**AlgebraicTree("(0)")
			self.assertEqual(res.evaluate(), 1)	
		
		for i in range(1,self.numberOfTestsPerLoop):
			randomValue = random.randint(2, 100)
			at1 = AlgebraicTree("(1/"+str(randomValue)+")")
			at12 = self.ats[0]**(at1)			
			self.assertEqual(at12.evaluate(),1)
		
if __name__ == '__main__':
	unittest.main()
